import {useStorage} from "@vueuse/core";

const TokenKey = 'Admin-Token';

const tokenStorage = useStorage(TokenKey, null);

export const getToken = () => tokenStorage.value;

export const setToken = (access_token) => (tokenStorage.value = access_token);

export const removeToken = () => (tokenStorage.value = null);
